import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Vuex from 'vuex';
import store from './store/index';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(Vuex);

require('/static/font-awesome/css/font-awesome.min.css');

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
