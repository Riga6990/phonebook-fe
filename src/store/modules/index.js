import auth from './auth/index';
import contacts from './contacts/index';

export default {
  auth,
  contacts
}