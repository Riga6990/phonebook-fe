import axios from 'axios';
import router from '../../../router';

export default {
  namespaced: true,
  state: {
    block: false,
    authorization: {
      token: '',
      user: 'pizda',
      result: false
    }
  },
  getters: {
    auth: (state) => state.authorization,
    block: (state) => state.block,
  },
  mutations: {
    SET_DATA_AUTH(state, payload) {
      state.authorization = {
        token: payload.token,
        user: payload.user,
        result: payload.result
      }
    },
    CLEAR_DATA_AUTH(state) {
      state.authorization = {
        token: '',
        user: '',
        result: false
      }
    },
    SET_BLOCK(state, payload) {
      state.block = payload
    }
  },
  actions: {
    async login({commit}, user) {
      commit('SET_BLOCK', true);
      console.log('login & password: => ', user);
      await axios.get('http://jsonplaceholder.typicode.com/posts ')
        .then(() => {
          commit('SET_DATA_AUTH', {
            token: 'ljflgdhj',
            user: 'Andy Riga',
            result: true
          });
          router.push('/contacts')
        })
        .catch(() => console.log('error'))
        .finally(() => commit('SET_BLOCK', false))
    },
    async logout({commit}) {
      commit('SET_BLOCK', true);
      await axios.get('http://jsonplaceholder.typicode.com/posts ')
        .then(() => {
          commit('CLEAR_DATA_AUTH');
          router.push('/')
        })
        .catch(() => console.log('error'))
        .finally(() => commit('SET_BLOCK', false))
    }
  }
}